import { useState } from 'react';
import './App.css';
// eslint-disable-next-line no-unused-vars
import Catalog from './components/catalog';
// eslint-disable-next-line no-unused-vars
import ToggleButton from './components/ToggleButton';

const App = () => {
  const listItems = [
    { name: 'Pepsi', price: 30, description: 'Gaseosa cola', stock: true },
    { name: 'Coca Cola', price: 30, description: 'Gaseosa cola', stock: false },
    { name: 'Paso de los Toros', price: 30, description: 'Gaseosa sin azucar', stock: true },
    { name: 'Mirinda', price: 30, description: 'Gaseosa de naranja', stock: false },
    { name: 'Sprite', price: 30, description: 'Gaseosa sabor lima limon', stock: true },
    { name: 'Secco', price: 30, description: 'Gaseosa sabor pomelo', stock: false },
    { name: 'Seven Up', price: 30, description: 'Gaseosa sabor lima limon', stock: true },
    { name: 'Fanta', price: 30, description: 'Gaseosa sabor naranja', stock: false },
    { name: 'Coca Cola Zero', price: 30, description: 'Gaseosa cola light', stock: true },
    { name: 'Pepsi Light', price: 30, description: 'Gaseosa cola light', stock: false },
    { name: 'Schweppes', price: 30, description: 'Gaseosa sabor pomelo', stock: true }
  ];

  const [toggle, setToggle] = useState(true)

  return (
    <div className="App">
      <h1>Catalogo</h1>
      <Catalog listItems={listItems} toggle={toggle}/>
      <div className="toggle-button-container">
        <ToggleButton toggleButton={() => setToggle(!toggle)} toggle={toggle} />
      </div>
    </div>
  );
}

export default App;
