// eslint-disable-next-line no-unused-vars
import Producto from '../Producto';
import './catalog.css';

const Catalog = ({ listItems, toggle }) => {
  return <div className="catalog">
    {
      listItems.filter(item => item.stock || toggle).map((item, key) => <Producto item={item} key={key} />)
    }
  </div>
}

export default Catalog;
