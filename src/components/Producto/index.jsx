import './Producto.css';

const Producto = ({ item }) => {
  return <div className="producto">
    <div>
      <div>
        <h3>{item.name}</h3>
      </div>
      <div>
        <h4>${item.price}</h4>
      </div>
      <div>
        <p>{item.description}</p>
      </div>
      <div>
        <div className={ item.stock ? 'non-stock-button unselectable' : 'stock-button unselectable' }>
          Stock: { item.stock ? 'SI' : 'NO'}
        </div>
      </div>
    </div>
  </div>
}

export default Producto;
