import './ToggleButton.css';

const ToggleButton = ({ toggleButton, toggle }) => {
  return <div className="button unselectable" onClick={toggleButton}>
    { toggle ? 'Mostrar sólo con stock' : 'Mostrar todo'}
  </div>
}

export default ToggleButton;
